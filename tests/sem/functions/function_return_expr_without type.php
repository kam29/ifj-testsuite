//compiler 6
//
<?php
declare(strict_types=1);

function fun () : void
{
    return 5;
}

$var = fun();

return 0;